<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index','uses' => 'IndexController@index']);


Route::post('login', ['as' => 'login','uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout','uses' => 'Auth\LoginController@logout']);

Route::get('password/remind', ['as'=>'password.remind','uses'=>'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/remind', ['as'=>'send.password.remind','uses'=>'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset/{token}', ['as'=>'password.reset', 'uses'=>'Auth\ResetPasswordController@showResetForm']);
Route::post('password/reset', ['as'=>'password.update','uses'=>'Auth\ResetPasswordController@reset']);


Route::group(['middleware' => ['auth'],'prefix' => 'home', 'as' => 'home.'], function() {
    Route::get('/', ['as' => 'index','uses' => 'Dashboard\DashboardController@index']);

    Route::get('profile', ['as' => 'profile','uses' => 'Dashboard\ProfileController@index']);
    Route::post('profile/details/update', ['as' => 'profile.update.details','uses' => 'Dashboard\ProfileController@updateDetails']);
    Route::post('profile/login-details/update', ['as' => 'profile.update.login-details','uses' => 'Dashboard\ProfileController@updateLoginDetails']);

    Route::get('user', ['as' => 'user.list','uses' => 'Dashboard\UsersController@index']);
    Route::get('user/create', ['as' => 'user.create','uses' => 'Dashboard\UsersController@create']);
    Route::post('user/create', ['as' => 'user.store','uses' => 'Dashboard\UsersController@store']);
    Route::get('user/{user_id}/edit', ['as' => 'user.edit','uses' => 'Dashboard\UsersController@edit']);
    Route::post('user/{user_id}/update/details', ['as' => 'user.update.details','uses' => 'Dashboard\UsersController@updateDetails']);
    Route::post('user/{user_id}/update/login-details', ['as' => 'user.update.login-details','uses' => 'Dashboard\UsersController@updateLoginDetails']);      //----------testar
    Route::delete('user/{user_id}/delete', ['as' => 'user.delete','uses' => 'Dashboard\UsersController@destroy']);

    Route::get('category', ['as' => 'category.index','uses' => 'Dashboard\CategoriesController@index']);
    Route::get('category/create', ['as' => 'category.create','uses' => 'Dashboard\CategoriesController@create']);
    Route::post('category/create', ['as' => 'category.store','uses' => 'Dashboard\CategoriesController@store']);
    Route::get('category/{user_id}/edit', ['as' => 'category.edit','uses' => 'Dashboard\CategoriesController@edit']);
    Route::post('category/{user_id}/update', ['as' => 'category.update','uses' => 'Dashboard\CategoriesController@update']);
    Route::delete('category/{user_id}/delete', ['as' => 'category.delete','uses' => 'Dashboard\CategoriesController@destroy']);

    Route::get('link', ['as' => 'link.index','uses' => 'Dashboard\LinksController@index']);
    Route::get('link/create', ['as' => 'link.create','uses' => 'Dashboard\LinksController@create']);
    Route::post('link/create', ['as' => 'link.store','uses' => 'Dashboard\LinksController@store']);
    Route::get('link/{user_id}/edit', ['as' => 'link.edit','uses' => 'Dashboard\LinksController@edit']);
    Route::post('link/{user_id}/update', ['as' => 'link.update','uses' => 'Dashboard\LinksController@update']);
    Route::delete('link/{user_id}/delete', ['as' => 'link.delete','uses' => 'Dashboard\LinksController@destroy']);
});

Route::get('/{code}', ['as' => 'url.redirect','uses' => 'IndexController@urlredirect']);
