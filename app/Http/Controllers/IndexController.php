<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function urlredirect($code)
    {
        $link = Link::where('short_url','=',$code)->firstOrFail();
        $link->increment('clicks');
        $url = $link->long_url;
        return redirect()->away($url);
    }

}
