<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Category;
use App\Models\Link;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $categories = Category::all();
        if(Auth::User()->type==2){
            $links = Link::where('client_id','=',Auth::User()->id)->get();
        }else{
            $links = Link::all();
        }


        return view('dashboard.admin', compact('users','categories','links'));
    }

}
