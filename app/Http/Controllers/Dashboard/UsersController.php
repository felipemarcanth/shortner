<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserDetailsRequest;
use App\Http\Requests\UpdateUserLoginDetailsRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peer_page = 15;
        $search = Input::get('search');
        $users = User::Query();
        if ($search <> "") {
            $users->where(function ($q) use ($search) {
                $q->where('name', "like", "%{$search}%");
            });
        }
        $users = $users->paginate($peer_page);
        if ($search) {
            $users->appends(['search' => $search]);
        }

        return view('dashboard.user.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->type!=1){
            return redirect()->back();
        }

        return view('dashboard.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        if(Auth::user()->type!==1){
            return redirect()->route('dashboard.user.list')->withErrors('Você não está autorizado para executar esta ação.');
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->type = $request->type;
        $user->save();


        return redirect()->route('home.user.list')->withSuccess('Usuário criado com sucesso!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        if(Auth::user()->type!==1){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }
        $edit = true;
        $user = User::find($user_id);

        return view('dashboard.user.edit',compact('edit', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDetails(UpdateUserDetailsRequest $request, $user_id)
    {
        if(Auth::user()->type!==1){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }

        $user = User::find($user_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;
        $user->save();

        return redirect()->back()->withSuccess('Usuário atualizado com sucesso!');
    }


    /**
     * Update login information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function updateLoginDetails(UpdateUserLoginDetailsRequest $request, $user_id)
    {
        if(Auth::user()->type!==1){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }

        $user = User::find($user_id);
        $user->password = $request->password;
        $user->save();

        return redirect()->back()->withSuccess('Usuário atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        if(Auth::user()->type!==1){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação');
        }
        if(Auth::user()->id == $user_id){
            return redirect()->back()->withErrors('Você não pode excluir esse usuário.');
        }
        $user = User::find($user_id);
        $user->delete();

        return redirect()->route('home.user.list')->withSuccess('Usuário excluido com sucesso!');
    }
}
