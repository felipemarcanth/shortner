<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peer_page = 15;
        $search = Input::get('search');

        $categories = Category::Query();
        if ($search <> "") {
            $categories->where(function ($q) use ($search) {
                $q->where('name', "like", "%{$search}%");
            });
        }
        $categories = $categories->paginate($peer_page);

        if ($search) {
            $categories->appends(['search' => $search]);
        }

        return view('dashboard.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->slug = $slug = Str::slug($request->name, '-');
        $category->save();

        return redirect()->route('home.category.index')->withSuccess('Categoria criada com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $category = Category::find($id);

        return view('dashboard.category.edit',compact('edit', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = $slug = Str::slug($request->name, '-');
        $category->save();
        return redirect()->back()->withSuccess('Categoria atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect()->route('home.category.index')->withSuccess('Categoria excluida com sucesso!');
    }
}
