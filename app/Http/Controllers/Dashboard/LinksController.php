<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\CreateLinkRequest;
use App\Http\Requests\UpdateLinkRequest;
use App\Models\Category;
use App\Models\Link;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peer_page = 15;
        $search = Input::get('search');
        $order = Input::get('order');

        $links = Link::Query();
        if(Auth::User()->type==2){
            $links->where('client_id','=',Auth::User()->id);
        }
        if ($search <> "") {
            $links->where(function ($q) use ($search) {
                $q->where('long_url', "like", "%{$search}%");
                $q->orwhere('name', "like", "%{$search}%");
            });
        }
        if($order <> ""){
            if($order==1){
                $links->orderBy('clicks','desc');
            }elseif($order==2){
                $links->orderBy('clicks','asc');
            }elseif($order==3){
                $links->orderBy('created_at','desc');
            }elseif($order==4){
                $links->orderBy('created_at','asc');
            }
        }else{
            $links->orderBy('created_at','desc');
        }
        $links = $links->paginate($peer_page);

        if ($search) {
            $links->appends(['search' => $search]);
        }

        return view('dashboard.link.list', compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::All();
        $clients = User::where('type','=',2)->get();
        return view('dashboard.link.add', compact('categories','clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLinkRequest $request)
    {
        $hash = Str::random(8);
        $aux = Link::where('short_url','=',$hash)->get();
        while($aux->count()>0){
            $hash = Str::random(8);
            $aux = Link::where('short_url','=',$hash)->get();
        }


        $link = new Link();
        $link->long_url = $request->long_url;
        $link->short_url = $hash;
        $link->user_id = Auth::User()->id;
        $link->category_id = $request->category_id;
        $link->client_id = $request->client_id;
        $link->name = $request->name;
        $link->save();

        return redirect()->route('home.link.index')->withSuccess('Link criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $link = Link::find($id);
        $categories = Category::All();
        $clients = User::where('type','=',2)->get();
        return view('dashboard.link.edit',compact('edit', 'link','categories','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLinkRequest $request, $id)
    {
        $link = Link::find($id);
        $link->long_url = $request->long_url;
        $link->category_id = $request->category_id;
        $link->client_id = $request->client_id;
        $link->name = $request->name;
        $link->save();
        return redirect()->back()->withSuccess('Link atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link = Link::find($id);
        $link->delete();

        return redirect()->route('home.link.index')->withSuccess('Link excluido com sucesso!');
    }
}
