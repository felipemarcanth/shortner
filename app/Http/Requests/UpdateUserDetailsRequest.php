<?php

namespace App\Http\Requests;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;

class UpdateUserDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::where('email','=',$this->request->get('email'))->first();
        $rules = [
            'email' => 'required|unique:users,email,'.$user->id,
            'name' => 'required',
            'type' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor preencha o campo nome',
            'email.required' => 'Por favor preencha o campo email',
            'email.unique' => 'Este email já esta em uso',
            'type.required' => 'Por favor selecione o tipo de usuário',
        ];
    }
}
