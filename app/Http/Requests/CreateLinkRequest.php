<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'long_url' => 'required|url',
            'category_id' => 'required',
            'client_id' => '',
            'name' => '',
        ];
    }

    public function messages()
    {
        return [
            'long_url.required' => 'Por favor preencha o campo url',
            'long_url.url' => 'A url informada não é válida',
            'category_id.required' => 'Por favor selecione a categoria',
        ];
    }
}
