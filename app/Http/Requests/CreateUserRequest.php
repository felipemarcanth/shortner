<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor preencha o campo nome',
            'email.required' => 'Por favor preencha o campo email',
            'email.unique' => 'Este email já esta em uso',
            'password.required' => 'Por favor preencha o campo senha',
            'password.confirmed' => 'A confirmação da senha não corresponde.',
            'type.required' => 'Por favor selecione o tipo de usuário',
        ];
    }
}
