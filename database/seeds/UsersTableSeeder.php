<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user= new \App\Models\User();
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = 'admin123';
        $user->type = 1;
        $user->save();
    }
}
