@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Olá!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ 'Você está recebendo este email, por que recebemos uma requisição de redefinição de senha para a sua conta' }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ 'Redefinir senha' }}
@endcomponent
@endisset

{{-- Outro Lines
@foreach ($outroLines as $line)--}}
{{ 'Este link de redefinir senha expira em 60 minutos' }}

{{--@endforeach--}}

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Saudações'),<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Se você estiver com problemas para clicar no botão \":actionText\", copie e cole a URL abaixo\n".
    'em seu navegador: [:actionURL](:actionURL)',
    [
        'actionText' => 'Redefinir senha',
        'actionURL' => $actionUrl,
    ]
)
@endslot
@endisset
@endcomponent
