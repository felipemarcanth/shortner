@extends('dashboard.layouts.master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
@stop

@section('body_class', 'skin-'. config('adminlte.skin', 'blue') .' sidebar-mini')

@section('content_header')
    <h1>
        {{ Auth::User()->name }}
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Home</li>
      </ol>
@endsection

@section('content')

<div class="row">
    @if(Auth::User()->type==1)
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>{{ $users->count() }}</h3>
                <p>Total de usuários</p>
            </div>
            <div class="icon">
                <i class="fa fa-cubes"></i>
            </div>

            <a href="{{ route('home.user.list') }}" class="small-box-footer">
                Ver todos usuários <i class="fa fa-arrow-circle-right"></i>
            </a>

        </div>
    </div>
    @endif
    @if(Auth::User()->type==1)
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>{{ $categories->count() }}</h3>
                <p>Total de categorias</p>
            </div>
            <div class="icon">
                <i class="fa fa-link"></i>
            </div>
            <a href="{{ route('home.category.index') }}" class="small-box-footer">
                Ver todas categorias <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    @endif

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-primary">
            <div class="inner">
                    <h3>{{  $links->count() }}</h3>
                <p>Total de links</p>
            </div>
            <div class="icon">
                <i class="fa fa-link"></i>
            </div>
            <a href="{{ route('home.link.index') }}" class="small-box-footer">
                Ver todos links <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

</div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/app.min.js') }}"></script>
@stop
