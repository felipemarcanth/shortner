<div class="panel panel-default">
    <div class="panel-heading">Detalhes do link</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Categoria</label>
                    <select name="category_id" id="category_id" class="form-control">
                        <option value="">Selecione</option>
                        @if($edit)
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{$link->category_id==$category->id?'selected="selected"':'' }}>{{$category->name}}</option>
                            @endforeach
                        @else
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{old('category_id')==$category->id?'selected="selected"':'' }}>{{$category->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name"
                           name="name" placeholder="informe um nome identificador" value="{{ $edit ? $link->name : old('name') }}">
                </div>
                <div class="form-group">
                    <label for="name">URL</label>
                    <input type="text" class="form-control" id="long_url"
                           name="long_url" placeholder="http://www.google.com" value="{{ $edit ? $link->long_url : old('long_url') }}">
                </div>
                <div class="form-group">
                    <label for="name">Cliente</label>
                    <select name="client_id" id="client_id" class="form-control">
                        <option value="">Selecione</option>
                        @if($edit)
                            @foreach($clients as $client)
                                <option value="{{$client->id}}" {{$link->client_id==$client->id?'selected="selected"':'' }}>{{$client->name}}</option>
                            @endforeach
                        @else
                            @foreach($clients as $client)
                                <option value="{{$client->id}}" {{old('client_id')==$client->id?'selected="selected"':'' }}>{{$client->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

            </div>


            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
