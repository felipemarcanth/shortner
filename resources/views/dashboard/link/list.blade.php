@extends('dashboard.layouts.master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
@endsection

@section('content_header')
<h1>
    Links
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Links</li>
</ol>
@endsection
@section('content')
@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        @if(Auth::User()->type==1)
        <a href="{{ route('home.link.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
            Adicionar Link
        </a>
        @endif
    </div>
    <div class="col-md-5 col-xs-3">

    </div>

    <form method="GET" action="" accept-charset="UTF-8" id="link-form">
        <div class="col-md-2 col-xs-3">
            <select name="order" id="order" class="form-control">
                <option value="">Ordenar</option>
                <option value="1">Mais cliques primeiro</option>
                <option value="2">Menos cliques primeiro</option>
                <option value="3">Data mais recente</option>
                <option value="4">Data mais antiga</option>
            </select>
        </div>
        <div class="col-md-3 col-xs-4">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ app('request')->input('search') }}" placeholder="Procure por links...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (app('request')->input('search') != '')
                        <a href="{{ route('home.link.index') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>

</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lista de links cadastrados</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                @if(Auth::User()->type==1)
                                    <th>Categoria</th>
                                    <th>Nome</th>
                                    <th>URL longa</th>
                                    <th>URL curta</th>
                                    <th>Usuário</th>
                                    <th>Cliente</th>
                                    <th>Cliques</th>
                                    <th>Criada em</th>
                                    <th class="text-center">Ações</th>
                                @else
                                    <th>Categoria</th>
                                    <th>Nome</th>
                                    <th>URL longa</th>
                                    <th>URL curta</th>
                                    <th>Cliques</th>
                                    <th>Criada em</th>
                                @endif
                            </tr>
                            @if (count($links))
                            @foreach ($links as $link)
                            <tr>
                                @if(Auth::User()->type==1)
                                    <td>{{ $link->category->name }}</td>
                                    <td>{{ $link->name }}</td>
                                    <td>{{ $link->long_url }}</td>
                                    <td><a href="{{ route('url.redirect',$link->short_url) }}" target="_blank">{{ route('url.redirect',$link->short_url) }}</a></td>
                                    <td>{{ $link->user->name }}</td>
                                    <td>{{ $link->client_id == null? '-': $link->client->name}}</td>
                                    <td>{{ $link->clicks }}</td>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($link->created_at)) }}</td>
                                    <td class="text-center">
                                        @if(Auth::User()->type==1)
                                        <a href="{{ route('home.link.edit', $link->id) }}" class="btn btn-primary btn-circle edit" title="Editar link"
                                            data-toggle="tooltip" data-placement="top">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </a>
                                        @endif
                                        @if(Auth::User()->type==1)
                                        <a href="{{ route('home.link.delete', $link->id) }}" class="btn btn-danger btn-circle" title="Excluir link"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            data-method="DELETE"
                                            data-confirm-title="Por favor confirme"
                                            data-confirm-text="Tem certeza que deseja excluir esse link?"
                                            data-confirm-delete="Sim">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                        @endif
                                    </td>
                                @else
                                    <td>{{ $link->category->name }}</td>
                                    <td>{{ $link->name }}</td>
                                    <td>{{ $link->long_url }}</td>
                                    <td><a href="{{ route('url.redirect',$link->short_url) }}" target="_blank">{{ route('url.redirect',$link->short_url) }}</a></td>
                                    <td>{{ $link->clicks }}</td>
                                    <td>{{ date('d/m/Y', strtotime($link->created_at)) }}</td>
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>Links não encontrados</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    {{ $links->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
    <script src="{{ asset('assets/delete.handler.js') }}"></script>

    <script>
        $("#order").change(function () {
            $("#link-form").submit();
        });
    </script>
@stop
