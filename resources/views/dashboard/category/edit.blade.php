@extends('dashboard.layouts.master')

@section('content_header')
    <h1>
        {{ $category->name }}
        <small>editar detalhes da categoria</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Categorias</li>
        <li class="active">Editar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')

<div class="nav-tabs-custom">
    <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#details" aria-controls="details" role="tab" data-toggle="tab">
            <i class="glyphicon glyphicon-th"></i>
            Detalhes
        </a>
    </li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="details">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                @if(Auth::User()->type==1)
                    <form action="{{route('home.category.update',$category->id)}}" method="post">
                    @csrf
                    @include('dashboard.category.partials.details')
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
</div>

@stop
