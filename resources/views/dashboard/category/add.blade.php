@extends('dashboard.layouts.master')

@section('content_header')
    <h1>
        Criar nova categoria
        <small>Detalhes da categoria</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Categorias</li>
        <li class="active">Criar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')
@if(Auth::User()->type==1)
    <form action="{{route('home.category.store')}}" method="POST">
    @csrf
        <div class="row">
            <div class="col-md-8">
                @include('dashboard.category.partials.details', ['edit' => false])
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                    Criar categoria
                </button>
            </div>
        </div>
    </form>
@endif
@stop

