<div class="panel panel-default">
    <div class="panel-heading">Detalhes do usuário</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name"
                           name="name" placeholder="Nome do usuário" value="{{ $edit ? $user->name : old('name') }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email"
                           name="email" placeholder="Email do usuário" value="{{ $edit ? $user->email : old('email') }}">
                </div>
                <div class="form-group">
                    <label for="email">Tipo</label>
                    <select name="type" id="type" class="form-control">
                        <option value="">Selecione o Tipo de usuário</option>
                        @if($edit)
                            <option value="1" {{($user->type==1 || old('type')==1)?'selected="selected"':'' }}>Funcionário</option>
                            <option value="2" {{($user->type==2 || old('type')==2)?'selected="selected"':'' }}>Cliente</option>
                        @else
                            <option value="1" {{old('type')==1?'selected="selected"':'' }}>Funcionário</option>
                            <option value="2" {{old('type')==2?'selected="selected"':'' }}>Cliente</option>
                        @endif
                    </select>
                </div>

                @if (!$edit)
                    <div class="form-group">
                        <label for="password">Senha</label>
                        <input type="text" class="form-control" id="password"
                               name="password" placeholder="" value="">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirmar senha</label>
                        <input type="text" class="form-control" id="password_confirmation"
                               name="password_confirmation" placeholder="" value="">
                    </div>
                @endif
            </div>


            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
