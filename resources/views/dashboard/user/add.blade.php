@extends('dashboard.layouts.master')

@section('content_header')
    <h1>
        Criar novo usuário
        <small>Detalhes do usuario</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Usuários</li>
        <li class="active">Criar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')
@if(Auth::User()->type==1)
    <form action="{{route('home.user.store')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-md-8">
            @include('dashboard.user.partials.details', ['edit' => false])
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i>
                Criar usuário
            </button>
        </div>
    </div>
    </form>
@endif
@stop

