@extends('dashboard.layouts.master')

@section('page-header')
<h1>
    Usuários
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Usuários</li>
</ol>
@endsection
@section('content')
@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        @if(Auth::User()->type==1)
        <a href="{{ route('home.user.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
            Adicionar usuário
        </a>
        @endif
    </div>
    <div class="col-md-5 col-xs-3"></div>

    <form method="GET" action="" accept-charset="UTF-8">
        <div class="col-md-2 col-xs-3">

        </div>
        <div class="col-md-3 col-xs-4">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ app('request')->input('search') }}" placeholder="Procure por usuários...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (app('request')->input('search') != '')
                        <a href="{{ route('home.user.list') }}" class="btn btn-danger" type="button" >
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    @endif
                </span>
            </div>
        </div>
    </form>

</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lista de usuários cadastrados</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th class="text-center">Ações</th>
                            </tr>
                            @if (count($users))
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->type == 1? 'Funcionário':'Cliente' }}</td>
                                <td class="text-center">
                                    @if(Auth::User()->type==1)
                                    <a href="{{ route('home.user.edit', $user->id) }}" class="btn btn-primary btn-circle edit" title="Editar usuário"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    @endif
                                    @if(Auth::User()->type==1)
                                    <a href="{{ route('home.user.delete', $user->id) }}" class="btn btn-danger btn-circle" title="Excluir usuário"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Por favor confirme"
                                        data-confirm-text="Tem certeza que deseja excluir esse usuário?"
                                        data-confirm-delete="Sim">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>Registros não foram encontrados</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
    <script src="{{ asset('assets/delete.handler.js') }}"></script>
@stop
