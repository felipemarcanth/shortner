<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                @can(Auth::check())
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                @endcan
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="">
                <a href="{{ route('home.index') }}">
                    <i class="fa fa-dashboard fa-fw"></i> <span>Home</span>
                </a>
            </li>
            @if(Auth::User()->type==1)
            <li class="">
                <a href="{{ route('home.user.list') }}">
                    <i class="fa fa-users fa-fw"></i> <span>Usuários</span>
                </a>
            </li>
            @endif
            @if(Auth::User()->type==1)
            <li class="">
                <a href="{{ route('home.category.index') }}">
                    <i class="fa fa-tags fa-fw"></i> <span>Categorias</span>
                </a>
            </li>
            @endif
            <li class="">
                <a href="{{ route('home.link.index') }}">
                    <i class="fa fa-link fw"></i> <span>Links</span>
                </a>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
