<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('home.index') }}" class="logo">
        <span class="logo-lg">Encurtador</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::User()->name }}</span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- Menu Body -->
                        <li class="user-body">
                            <a href="{{ route('home.profile') }}" class="btn btn-default btn-block">
                               <i class="fa fa-user"></i>
                               Meu Perfil
                            </a>


                            <a href="{{ route('logout') }}" class="btn btn-default btn-block">
                               <i class="fa fa-sign-out"></i>
                               Sair
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
